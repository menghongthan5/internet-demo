
  let sTime, mTime, hTime, dTime;
  let myTime, countTime = 0;
  let tStart, tStop, is_stop = "", totalSecond = "";
  
  let timeStart, timeStop;
  document.getElementById('Pay').addEventListener('click', myButton);
  var localTime = setInterval(() => {
    var timer = new Date();
    var Hours = timer.getHours() < 10 ? '0' + timer.getHours() : timer.getHours();
    var Minutes = timer.getMinutes() < 10 ? '0' + timer.getMinutes() : timer.getMinutes();
    var Seconds = timer.getSeconds() < 10 ? '0' + timer.getSeconds() : timer.getSeconds();
    if (countTime == 1 && myTime.getHours() == timer.getHours() && myTime.getMinutes() == timer.getMinutes() && myTime.getSeconds() == timer.getSeconds()) {
      countTime = 0;
      clickStop();
    }
  }, 1000);
  
  function myButton() {
    var getBtnValue = document.getElementById('Pay').value;
    if (getBtnValue == 'Start') {
      clickStart();
      document.getElementById('Pay').value = "Stop";
      document.getElementById('Pay').innerHTML = `<i class="far fa-stop-circle"> Stop</i>`;
    }
    else if (getBtnValue == 'Stop') {
      clickStop();
  
    }
    else if (getBtnValue = 'Clear') {
      Clear();
    }
  }
  function clickStart() {
  
    tStart = new Date();
    sTime = tStart.getSeconds();
    mTime = tStart.getMinutes();
    hTime = tStart.getHours();
    var myStartS = sTime < 10 ? '0' + sTime : sTime;
    var myStartM = mTime < 10 ? '0' + mTime : mTime;
    var myStartH = hTime < 10 ? '0' + hTime : hTime;
  
    document.getElementById('startTime').innerHTML = ` <i class="far fa-play-circle"> Start at: ${myStartH + ":" + myStartM}</i> `;
  }
  function clickStop() {
  
    if (!is_stop) {
      is_stop = true;
      tStop = new Date()
      let Th = (tStop.getHours() - tStart.getHours()) * 60;
      let Mh = (tStop.getMinutes() - tStart.getMinutes());
      let duration = Th + Mh ;
      //duration = Math.floor;
      sTime = tStop.getSeconds();
      mTime = tStop.getMinutes();
      hTime = tStop.getHours();
      var myStopS = sTime < 10 ? '0' + sTime : sTime;
      var myStopM = mTime < 10 ? '0' + mTime : mTime;
      var myStopH = hTime < 10 ? '0' + hTime : hTime;
  
      document.getElementById('stopTime').innerHTML = `<i class="far fa-stop-circle"> Stop at: ${myStopH + ":" + myStopM}</i>`;
      document.getElementById('minT').innerHTML = `<i class="far fa-clock"> Minutes ${duration}</i>`;
      document.getElementById('payT').innerHTML = `<i class="far fa-money-bill-alt"> ${(Payment(duration)) + '\u17DB'}</i>`;
      document.getElementById('Pay').value = "Clear";
      document.getElementById('Pay').innerHTML = `<i class="fas fa-trash"> Clear</i>`;
    }
  
  }
  function Clear() {
    is_stop = false;
    document.getElementById('startTime').innerHTML = `<i class="far fa-play-circle"> Start at: 00:00</i>`;
    document.getElementById('stopTime').innerHTML = `<i class="far fa-stop-circle"> Stop at: 00:00</i>`;
    document.getElementById('minT').innerHTML = `<i class="far fa-clock"> Minutes</i>`;
    document.getElementById('payT').innerHTML = `<i class="far fa-money-bill-alt"> Rial</i>`;
    document.getElementById('Pay').value = "Start";
    document.getElementById('Pay').innerHTML = `<i class="fas fa-play"> Start</i>`;
  
  }
  function Payment(duration) {
    if (duration <= 15) {
      return 500;
    } else if (duration > 15 && duration <= 30) {
      return 1000;
    }
    else if (duration > 30 && duration <= 60) {
      return 1500;
    }
    else if (duration <= 60) {
      return 1500;
    }
    else if(duration > 60 && duration <= 120) {
      return 2000;
    }
    else if(duration > 120 && duration <= 160) {
      return 2500;
    }
  }
  
  const formatter = new Intl.NumberFormat('km-KH', {
    style: 'currency',
    currency: 'KHR',
    minimumFractionDigits: 0
  });
  